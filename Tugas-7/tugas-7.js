//Soal 1
console.log("Output Nomor 1 (Release 0):");
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4;
    this.cold_blooded = false;
  }
  get cnam() {
    return this.name;
  }
  set cnam(x) {
    this.name = x;
  }}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

console.log("Output Nomor 1  (Release 1):");

// Code class Ape dan class Frog di sini

class Ape extends Animal {
 constructor(name) {
   super(name);
   this.name = name;
   this.legs = 2;
 }
 yell() {
   return "Auooo";
 }
}

class Frog extends Animal {
 constructor(name) {
   super(name);
   this.name = name;
 }
 jump() {
   return "hop hop";
 }
}

var sungokong = new Ape("kera sakti")
console.log("Suara Kera", sungokong.yell()); // Auoo

var kodok = new Frog("buduk")
console.log("Suara Lompat Kodok", kodok.jump())// "hop hop"

//Soal 2
console.log("===========================");
console.log("Output Nomor 2: ");

class Clock {
  constructor({ template }) {
    this.template = template;
  }

  render() {
    let date = new Date();

    let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    let mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    let secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;

    let output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);

    console.log(output);
  }

  stop() {
    clearInterval(this.timer);
  }

  start() {
    this.render();
    this.timer = setInterval(() => this.render(), 1000);
  }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();
