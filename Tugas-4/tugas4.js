//Soal 1
console.log("Output Soal Nomor 1 :");
var i = 2;
  console.log("LOOPING PERTAMA");
while(i < 21) {

  console.log(i + " - I Love Coding");
  i=i+2;
}

var i = 20;
  console.log("LOOPING KEDUA");
while(i > 0) {

  console.log(i + " - I will become a frontend developer");
  i=i-2;
}

//Soal 2
console.log("=====================");
console.log("Output Soal Nomor 2 :");

var i = 1;
for(i; i <= 20; i++) {
  if(i % 2 == 1 && i % 3 == 0) {
    console.log(i + " - I Love Coding");
  }
  else if(i % 2 == 0){
    console.log(i + " - Berkualitas");
  }
  else if (i % 2 == 1) {
    console.log(i + " - Santai");
  }
}

//Soal 3
console.log("=====================");
console.log("Output Soal Nomor 3 :");

var row = 7;

for (var i = 1; i <= row; i++) {
	var hasil = '';
	for (var j = 1; j <= i; j++) {
    hasil += "#";
	}
	console.log(hasil);
}

//Soal 4
console.log("=====================");
console.log("Output Soal Nomor 4 :");
var kalimat="saya sangat senang belajar javascript"
var kalimatarray = kalimat.split(" ");
console.log(kalimatarray);

//Soal 5
console.log("=====================");
console.log("Output Soal Nomor 5 :");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];

daftarBuah.sort()
var i = 0;
for (i; i < daftarBuah.length; i++) {
  console.log( daftarBuah[i]);
}
