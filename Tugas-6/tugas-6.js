
// Soal 1
console.log("====================");
console.log("Output Soal  Nomor 1 : ");
let phi = 3.14
const luas = (r) => {
  return  phi * r * r
}
console.log("Luas Lingkarannya : ", luas(6));

const keliling = (r) => 2* phi * r;
console.log("keliling Lingkarannya : ", keliling(6));


// Soal 2
console.log("====================");
console.log("Output Soal  Nomor 2 : ");
let kalimat = ""

const tambahKalimat = () => {
  const kataPertama = 'saya'
  const kataKedua = 'adalah'
  const kataKetiga = 'seorang'
  const kataKeempat = 'frontend'
  const kataKelima = 'developer'
  const kalimat = `${kataPertama} ${kataKedua} ${kataKetiga} ${kataKeempat} ${kataKelima}`
  return  kalimat;
}

console.log(tambahKalimat());

// Soal 3
console.log("====================");
console.log("Output Soal  Nomor 3 : ");

const newFunction = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return
    }
  }
}
//Driver Code
newFunction("William", "Imoh").fullName();

// Soal 4
console.log("====================");
console.log("Output Soal  Nomor 4 : ");

const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject

console.log(firstName, lastName, destination, occupation)

// Soal 5
console.log("====================");
console.log("Output Soal  Nomor 5 : ");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
 combined = [...west, ...east]
//Driver Code
console.log(combined)
