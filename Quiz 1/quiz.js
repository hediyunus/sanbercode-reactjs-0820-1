
// Soal 2
console.log("====================");
console.log("Luas Lingkaran : ");


function lingkaran(r, r){
  return phi * r * r;
}
var phi = 3.14
console.log(lingkaran(6, 6))

console.log("====================");
console.log("Luas Segitiga : ");


function segitiga(alas, tinggi){
  return alas * tinggi / 2;
}

console.log(segitiga(6, 6))

console.log("====================");
console.log("Luas Persegi : ");


function persegi(sisi){
  return sisi * sisi;
}

console.log(persegi(6))


//Soal 13
console.log("=====================");
console.log("Output ");
var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"];
daftarAlatTulis.sort()
var i = 0;

while(i < daftarAlatTulis.length) {
  console.log( daftarAlatTulis[i]);
  i++;
}

//Soal 14
var pesertaLomba= [["Budi", "Pria", "172cm"], ["Susi", "Wanita", "162cm"], ["Lala", "Wanita", "155cm"], ["Agung", "Pria", "175cm"]]

var dataPesertaLomba = pesertaLomba.map((el) =>{
  var objectPesertaLomba = {
    nama: el[0],
    jenisKelamin: el[1],
    tinggi: el[2]
  }
  return objectPesertaLomba
})

console.log(dataPesertaLomba)


//Soal 15
var daftarNama = []
function TambahNama(nama, jeniskelamin) {
  daftarNama.push({
    Nama : nama,
    JenisKelamin : jeniskelamin,
    panggilan: jeniskelamin === "L" ? "Bapak" : (jeniskelamin === "P" ? "Ibu" : "Undefined")  })

}

TambahNama("Asep", "L")
TambahNama("Siti", "P")
TambahNama("Yeni", "P")
TambahNama("Rudi", "L")
TambahNama("Adit", "L")

for (var i=0; i < daftarNama.length ;i++ ){
  var number = i+1
  console.log(number+ ". " + daftarNama[i].panggilan + " " +daftarNama[i].Nama)
}
