//Soal 1
console.log("Output Soal Nomor 1: ");
function halo() {
  var x = "Halo Sanbers!"
  return x;
}
console.log(halo())

// Soal 2
console.log("====================");
console.log("Output Soal Nomor 2: ");
function kalikan(num1, num2){
  return num1 * num2;
}
var num1 = 12
var num2 = 4

var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48

//Soal 3
console.log("====================");
console.log("Output Soal Nomor 3: ");
function introduce(name, age, address, hobby) {
  return "Nama Saya " + name + ", umur saya " + age +" tahun, alamat saya di "+ address +", dan saya punya hobby yaitu " + hobby;
}
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!"

//Soal 4
console.log("====================");
console.log("Output Soal Nomor 4: ");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992]
var object = {
    nama : arrayDaftarPeserta[0],
    JenisKelamin : arrayDaftarPeserta[1],
    hobi : arrayDaftarPeserta[2],
    hobby : arrayDaftarPeserta[3],
}

console.log(object)

//Soal 5
console.log("====================");
console.log("Output Soal Nomor 5: ");

var DaftarBuah = [
  {
   nama: "strawberry",
  warna: "merah",
adaBijinya  : "tidak",
  harga: 9000
}, {
    nama: "jeruk",
    warna: "oranye",
    adaBijinya: "ada",
    harga: 8000
},
{
  nama: "Semangka",
    warna: "Hijau & Merah",
    adaBijinya: "ada",
    harga: 10000
},
{
    nama: "Pisang",
    warna: "Kuning",
    adaBijinya: "tidak",
    harga: 5000
}
]

// console.log(DaftarBuah);
console.log("Data Pertama : ", DaftarBuah[0]);

//Soal 6
console.log("====================");
console.log("Output Soal Nomor 6: ");

var dataFilm = []

function film(nama, durasi, genre, tahun) {
  var data = {
    Nama : nama,
    durasi : durasi,
    genre: genre,
    tahun: tahun
  }
  return data;
}
dataFilm = [film("Avengers: end game", 120, "Action", 2019)];


console.log(dataFilm);
